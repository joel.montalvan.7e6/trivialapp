package com.example.tontoolisto

object TrivialHandlerDeportes {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "¿Que equipo de fútbol fue traicionado por Kylian Mbappe en el mercado de fichajes de 2022?",
                "FC BARCELONA",
                "REAL MADRID",
                "LIVERPOOL",
                "BAYERN DE MUNICH",
                "REAL MADRID"
            ),

            QuestionTrivial(
                "¿Que jugador famoso de baloncesto participó en la película animada Space Jam (1996)?",
                "KOBE BRYANT",
                "LEBRON JAMES",
                "MICHAEL JORDAN",
                "STEPHEN CURRY",
                "MICHAEL JORDAN"
            ),

            QuestionTrivial(
                "¿Quién ganó el Dakar en 2018?",
                "FERNANDO ALONSO",
                "RAFA NADAL",
                "CRISTIANO RONALDO",
                "CARLOS SAINZ",
                "CARLOS SAINZ"
            ),
            QuestionTrivial(
                "¿Quién es el jugador con más Balones de Oro en la historia del fútbol?",
                "LEO MESSI",
                "CRISTIANO RONALDO",
                "ANDRES INIESTA",
                "LUKA MODRIC",
                "LEO MESSI"
            ),
            QuestionTrivial(
                "¿Qué torneo ganó el jugador español de tenis Carlos Alcaraz por primera vez en su carrera profesional?",
                "OPEN DE AUSTRALIA",
                "ROLAND GARROS",
                "CAMPEONATO DE WIMBLEDON",
                "OPEN DE USA",
                "OPEN DE USA"
            )
        )
    }
}