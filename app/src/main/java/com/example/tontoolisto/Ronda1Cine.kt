package com.example.tontoolisto


import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import com.example.tontoolisto.databinding.FragmentRonda1CineBinding

class Ronda1Cine : Fragment(), OnClickListener {

    private lateinit var tquestion: TextView
    private lateinit var btanswer1: Button
    private lateinit var btanswer2: Button
    private lateinit var btanswer3: Button
    private lateinit var btanswer4: Button

    private var currentRoundIndex = 0
    private val listOfQuestion = TrivialHandler.getQuestions()
    private lateinit var currentRound:QuestionTrivial
    private var score = 0

    lateinit var binding: FragmentRonda1CineBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentRonda1CineBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var timer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.MyProgress.progress= (millisUntilFinished/1000).toInt()
//DO SOMETHING
            }
            override fun onFinish() {
//DO SOMETHING
            }
        }.start()

        tquestion = view.findViewById(R.id.pregunta_ronda)
        btanswer1 = view.findViewById(R.id.question1)
        btanswer2 = view.findViewById(R.id.question2)
        btanswer3 = view.findViewById(R.id.question3)
        btanswer4 = view.findViewById(R.id.question4)

        btanswer1.setOnClickListener(this)
        btanswer2.setOnClickListener(this)
        btanswer3.setOnClickListener(this)
        btanswer4.setOnClickListener(this)

        showQuestions()
    }

    private fun showQuestions() {
        if (currentRoundIndex >= listOfQuestion.size-1){
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial, ResultScreen())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }
        currentRound = listOfQuestion[currentRoundIndex++]
        tquestion.text = currentRound.question
        btanswer1.text = currentRound.answer1
        btanswer2.text = currentRound.answer2
        btanswer3.text = currentRound.answer3
        btanswer4.text = currentRound.answer4
    }

    @SuppressLint("ResourceType")
    override fun onClick(v: View?) {
        val answerSelected = when (v?.id){
            R.id.question1 -> currentRound.answer1
            R.id.question2 -> currentRound.answer2
            R.id.question3 -> currentRound.answer3
            else -> currentRound.answer4
        }
        if (answerSelected == currentRound.aCorrect){
            score++
        }
        showQuestions()
    }
}



