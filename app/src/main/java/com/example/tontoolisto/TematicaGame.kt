package com.example.tontoolisto

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tontoolisto.databinding.FragmentTematicaGameBinding

class TematicaGame : Fragment() {
    lateinit var binding: FragmentTematicaGameBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTematicaGameBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
        
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tematica1.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Cine())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }

        binding.tematica2.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Ciencia())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }

        binding.tematica3.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Historia())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }

        binding.tematica4.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Arte())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }
        binding.tematica5.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Deportes())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }

        binding.tematica6.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1Geografia())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }

        binding.tematica7.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,Ronda1CulturaGeneral())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }

        }
    }
}