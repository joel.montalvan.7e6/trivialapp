package com.example.tontoolisto

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tontoolisto.databinding.FragmentRankingGameBinding


class RankingGame : Fragment() {
    lateinit var binding: FragmentRankingGameBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRankingGameBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.backMenuButton.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,MenuInicial())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
    }
}