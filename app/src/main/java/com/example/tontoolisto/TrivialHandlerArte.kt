package com.example.tontoolisto

object TrivialHandlerArte {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "En mitología griega, ¿quién traslada a los muertos hacia el inframundo?",
                "ORFEO",
                "CARONTE",
                "MINOS",
                "TALOS",
                "CARONTE"
            ),

            QuestionTrivial(
                "¿Quién pintó \"La Gioconda\"?",
                "DONATELLO",
                "RAPHAEL",
                "LEONARDO DA VINCI",
                "MIGUEL ANGEL",
                "LEONARDO DA VINCI"
            ),

            QuestionTrivial(
                "¿La obra llamada \"Guernica\" es de…",
                "DIEGO RIVERA",
                "SALVADOR DALÍ",
                "HENRI MATISSE",
                "PABLO PICASSO",
                "PABLO PICASSO"
            ),
            QuestionTrivial(
                "¿Como se llama la saga de la famosas escritora J. K ROWLING?",
                "HARRY POTTER",
                "LOS JUEGOS DEL HAMBRE",
                "DIVERGENTE",
                "EL CORREDOR DEL LABERINTO",
                "HARRY POTTER"
            )
        )
    }
}