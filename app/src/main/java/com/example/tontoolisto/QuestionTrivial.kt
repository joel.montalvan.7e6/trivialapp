package com.example.tontoolisto

data class QuestionTrivial(val question:String,
                           val answer1:String,
                           val answer2:String,
                           val answer3:String,
                           val answer4:String,
                           val aCorrect:String
                           )
