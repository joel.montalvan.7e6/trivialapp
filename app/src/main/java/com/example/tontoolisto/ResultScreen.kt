package com.example.tontoolisto

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.tontoolisto.databinding.FragmentResultScreenBinding

class ResultScreen : Fragment() {
    lateinit var binding: FragmentResultScreenBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentResultScreenBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.returnMenuButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial, MenuInicial())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.seeRankingButton.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial, RankingGame())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.shareButton.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            startActivity(Intent.createChooser(shareIntent, "Share via"))
        }

    }
}