package com.example.tontoolisto

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tontoolisto.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        lateinit var binding: ActivityMainBinding

        Thread.sleep(2000)
        setTheme(R.style.Theme_TontoOListo)

        super.onCreate(savedInstanceState)

        if (supportActionBar != null){
            supportActionBar?.hide()
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentMenu_Inicial,MenuInicial())
            setReorderingAllowed(true)
            addToBackStack("name")
            commit()
        }
    }
}