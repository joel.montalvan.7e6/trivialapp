package com.example.tontoolisto

object TrivialHandlerCiencia {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "¿Quien fue la primera mujer en ganar un premio Nobel?",
                "SAMANTHA LAYER",
                "MARIE CURIE",
                "PAMELA FANCY",
                "ANABELLE MENCHER",
                "MARIE CURIE"
            ),

            QuestionTrivial(
                "¿Cuál es la capa más caliente de nuestro planeta?",
                "NUCLEO",
                "MANTO",
                "CORTEZA",
                "MAGNÉTOCA",
                "NUCLEO"
            ),

            QuestionTrivial(
                "¿Cuál es la unidad básica de un elemento químico?",
                "MOLÈCULA",
                "SUB ÁTOMO",
                "CÉLULA",
                "ÁTOMO",
                "ÁTOMO"
            ),
            QuestionTrivial(
                "¿Cuál es el primer paso del método científico?",
                "PLANEAR EN GRUPO LA TEORIA",
                "PROBAR LAS DIFERENTES TEORIAS",
                "IDENTIFICAR EL PROBLEMA",
                "PENSAR LOS DIFERENTES PASOS",
                "IDENTIFICAR EL PROBLEMA"
            ),
            QuestionTrivial(
                "¿Qué contiene el núcleo de una célula?",
                "LISOSOMAS",
                "ADN",
                "RIBOSOMAS",
                "ORGÁNULOS",
                "ADN"
            )
        )
    }
}