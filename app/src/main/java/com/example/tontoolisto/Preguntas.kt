package com.example.tontoolisto

object preguntas {
    val preguntasDeCine: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿A que dia viaja Marty en Regreso al futuro 2? ",
            "f_6 DE ENERO DE 2030",
            "f_14 DE JULIO DE 2016",
            "c_21 DE OCTUBRE DE 2015",
            "f_4 DE NOVIEMBRE DE 2020"
        ),
        mutableListOf(
            "¿Que actor fue el último en interpretar a Superman?",
            "f_NICOLAS CAGE",
            "f_TOM WELLING",
            "c_HENRY CAVILL",
            "f_CHRISTOPHER REEVE"
        ),
        mutableListOf(
            "¿Quien dirigio la pelicula Titanic (1998)?",
            "c_JAMES CAMERON",
            "f_CHRISTOPHER NOLAN",
            "f_STEVEN SPIELBERG",
            "f_QUENTIN TARANTINO"
        ),
        mutableListOf(
            "¿Quien dijo la mítica frase “Un gran poder, conlleva una gran responsabilidad”?",
            "f_BRUCE WAYNE / BATMAN",
            "f_PETER PARKER / SPIDER-MAN",
            "c_BENJAMIN FRANKLIN PARKER",
            "f_CLARK KENT / SUPERMAN"
        ),
        mutableListOf(
            "¿Cual es la película que más ha recaudado en la historia del cine?",
            "f_SPIDER-MAN: NO WAY HOME",
            "f_STAR WARS: EPISODE VII",
            "f_VENGADORES: ENDGAME",
            "c_AVATAR"
        ),
        mutableListOf(
            "En Harry Potter, ¿Qué hechizo debes conjurar para arreglar unas gafas?",
            "c_OCULUS REPARO",
            "f_OCULUS REPARADO",
            "f_GLAFULUS ARREGLO",
            "f_CHIMICHANGLUS OCULOREPAR"
        ),
        mutableListOf(
            "¿De qué metal están hechas las garras de Lobezno?",
            "f_HIERRO",
            "f_VIBRANIUM",
            "c_ADAMANTIUM",
            "f_ACERO"
        ),
        mutableListOf(
            "En 'The Walking Dead' ¿Cómo se llama el bate de Negan?",
            "f_LUBELLY",
            "c_LUCILLE",
            "f_VAMPYRE",
            "f_MAX POWER"
        ),
        mutableListOf(
            "En Star Wars ¿Cuál es el planeta de Luke Skywalker?",
            "f_ALDERAAN",
            "f_NABOO",
            "f_CORUSCANT",
            "c_TATOOINE"
        ),
        mutableListOf(
            "¿Por si no nos vemos luego...",
            "f_(...)NO TE OLVIDES EL PARAGUA",
            "f_(...)RECUERDA COMPRAR LECHE",
            "c_(...)BUENOS DÍAS, BUENAS TARDES, BUENAS NOCHES",
            "f_(...)TODA MI ESPADA, LLEVALA POR MI"
        ),
        mutableListOf(
            "¿En qué año se estrenó la mítica película Jurassic Park?",
            "c_1993",
            "f_1991",
            "f_1995",
            "f_1997"
        ),
        mutableListOf(
            "¿Cómo se llama la madre de Simba?",
            "f_NALA",
            "c_SARABI",
            "f_OKOYE",
            "f_MINERVA"
        )
    )

    val preguntasDeCiencia: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Quien fue la primera mujer en ganar un premio Nobel? ",
            "f_SAMANTHA LAYER",
            "c_MARIE CURIE",
            "f_PAMELA FANCY",
            "f_ANABELLE MENCHER"
        ),
        mutableListOf(
            "¿Cuál es la capa más caliente de nuestro planeta?",
            "c_NUCLEO",
            "f_MANTO",
            "f_CORTEZA",
            "f_MAGNÉTOCA"
        ),
        mutableListOf(
            "¿Cuál es la unidad básica de un elemento químico?",
            "f_MOLÈCULA",
            "f_SUB ÁTOMO",
            "f_CÉLULA",
            "c_ÁTOMO"
        ),
        mutableListOf(
            "¿Cuál es el primer paso del método científico?",
            "f_PLANEAR EN GRUPO LA TEORIA",
            "f_PROBAR LAS DIFERENTES TEORIAS",
            "c_IDENTIFICAR EL PROBLEMA",
            "f_PENSAR LOS DIFERENTES PASOS"
        ),
        mutableListOf(
            "¿Qué contiene el núcleo de una célula?",
            "f_LISOSOMAS",
            "c_ADN",
            "f_RIBOSOMAS",
            "f_ORGÁNULOS"
        )
    )
    val preguntasDeHistoria: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "La Guerra Fría fue entre Estados Unidos y ¿qué otra potencia mundial?",
            "f_ITALIA",
            "c_LA UNIÓN SOVIÉTICA",
            "f_RUSIA",
            "f_ALEMANIA"
        ),
        mutableListOf(
            "¿Quién fue la primera ministra de la Gran Bretaña?",
            "f_QUEEN ELIZABETH II",
            "f_FLORENCE NIGHTINGALE",
            "c_MARGARET THATCHER",
            "f_JANE AUSTEN"
        ),
        mutableListOf(
            "¿De qué partido era líder Adolf Hitler?",
            "f_FRANQUISTA",
            "f_URSS",
            "f_COMUNISTA",
            "c_NAZI"
        ),
        mutableListOf(
            "¿Qué continente fue devastado por la peste negra en el siglo XIV?",
            "c_EUROPA",
            "f_ASIA",
            "f_ÁFRICA",
            "f_AMÉRICA"
        ),
        mutableListOf(
            "¿En qué año comenzó la Primera Guerra Mundial?",
            "f_1913",
            "c_1914",
            "f_1917",
            "f_1915"
        )
    )
    val preguntasDeArte: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "En mitología griega, ¿quién traslada a los muertos hacia el inframundo?",
            "f_ORFEO",
            "c_CARONTE",
            "f_MINOS",
            "f_TALOS"
        ),
        mutableListOf(
            "¿Quién pintó \"La Gioconda\"?",
            "f_DONATELLO",
            "f_RAPHAEL",
            "c_LEONARDO DA VINCI",
            "f_MIGUEL ANGEL"
        ),
        mutableListOf(
            "¿La obra llamada \"Guernica\" es de…",
            "f_DIEGO RIVERA",
            "f_SALVADOR DALÍ",
            "f_HENRI MATISSE",
            "c_PABLO PICASSO"
        ),
        mutableListOf(
            "¿Como se llama la saga de la famosas escritora J. K ROWLING?",
            "c_HARRY POTTER",
            "f_LOS JUEGOS DEL HAMBRE",
            "f_DIVERGENTE",
            "f_EL CORREDOR DEL LABERINTO"
        )
    )
    val preguntasDeDeportes: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Que equipo de fútbol fue traicionado por Kylian Mbappe en el mercado de fichajes de 2022?",
            "f_FC BARCELONA",
            "c_REAL MADRID",
            "f_LIVERPOOL",
            "f_BAYERN DE MUNICH"
        ),
        mutableListOf(
            "¿Que jugador famoso de baloncesto participó en la película animada Space Jam (1996)?",
            "f_KOBE BRYANT",
            "f_LEBRON JAMES",
            "c_MICHAEL JORDAN",
            "f_STEPHEN CURRY"
        ),
        mutableListOf(
            "¿Quién ganó el Dakar en 2018?",
            "f_FERNANDO ALONSO",
            "f_RAFA NADAL",
            "f_CRISTIANO RONALDO",
            "c_CARLOS SAINZ"
        ),
        mutableListOf(
            "¿Quién es el jugador con más Balones de Oro en la historia del fútbol?",
            "c_LEO MESSI",
            "f_CRISTIANO RONALDO",
            "f_ANDRES INIESTA",
            "f_LUKA MODRIC"
        ),
        mutableListOf(
            "¿Qué torneo ganó el jugador español de tenis Carlos Alcaraz por primera vez en su carrera profesional?",
            "f_OPEN DE AUSTRALIA",
            "f_ROLAND GARROS",
            "f_CAMPEONATO DE WIMBLEDON",
            "c_OPEN DE USA"
        )
    )
    val preguntasDeGeografia: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Cual es el país más poblado del mundo?",
            "f_RUSIA",
            "c_CHINA",
            "f_ESTADOS UNIDOS",
            "f_BRASIL"
        ),
        mutableListOf(
            "¿Cómo se llama el pozo más profundo del mundo localizado en Rusia, el cual fue perforado hacia el interior de la tierra?",
            "f_POZO DE VRTOGLAVICA",
            "f_POZO DE JOIDA",
            "c_POZO DE KOLA",
            "f_POZO DE EZEIZA"
        ),
        mutableListOf(
            "¿En qué continente se encuentran las montañas más altas del mundo?",
            "f_EUROPA",
            "f_AMÉRICA",
            "f_ÁFRICA",
            "c_ASIA"
        ),
        mutableListOf(
            "¿Cuál es el país más pequeño del mundo?",
            "c_SEALAND",
            "f_EL VATICANO",
            "f_MONACO",
            "f_TUVALU"
        ),
        mutableListOf(
            "¿Cuál es la capital de Armenia?",
            "f_TAIWAN ",
            "c_EREVÁN",
            "f_REVAN",
            "f_MATALI"
        )
    )
    val preguntasDeVideojuegos: MutableList<MutableList<String>> = mutableListOf(
        mutableListOf(
            "¿Cuál fue el primer videojuego?",
            "f_TETRIS",
            "c_PONG",
            "f_PAC-MAN",
            "f_SNAKE GAME"
        ),
        mutableListOf(
            "¿Cómo se llama el protagonista de la saga ‘Halo’?",
            "f_CABO MUERTE",
            "f_COMANDANTE TOTAL",
            "c_JEFE MAESTRO",
            "f_JOHN MUERTE"
        ),
        mutableListOf(
            "¿Cuál de estos videojuegos ha desarrollado Rockstar?",
            "f_ASSASSIN’S CREED",
            "f_THE LAST OF US",
            "f_CALL OF DUTY",
            "c_GRAND THEFT AUTO V"
        ),
        mutableListOf(
            "¿De qué color es la corbata de Donkey Kong?",
            "c_ROJA",
            "f_VERDE",
            "f_AZUL",
            "f_NARANJA"
        ),
        mutableListOf(
            "¿Cómo se llamó el evento de Fortnite que presentó el rapero Travis Scott?",
            "f_IMAGINATIONLAND",
            "f_TEMPLE CRAZY",
            "c_ASTRONOMICAL",
            "f_RIFT TOUR"
        )
    )


}