package com.example.tontoolisto

object TrivialHandlerVideojuegos {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "¿Cuál fue el primer videojuego?",
                "TETRIS",
                "PONG",
                "PAC-MAN",
                "SNAKE GAME",
                "PONG"
            ),

            QuestionTrivial(
                "¿Cómo se llama el protagonista de la saga ‘Halo’?",
                "CABO MUERTE",
                "COMANDANTE TOTAL",
                "JEFE MAESTRO",
                "JOHN MUERTE",
                "JEFE MAESTRO"
            ),

            QuestionTrivial(
                "¿Cuál de estos videojuegos ha desarrollado Rockstar?",
                "ASSASSIN’S CREED",
                "THE LAST OF US",
                "CALL OF DUTY",
                "GRAND THEFT AUTO V",
                "GRAND THEFT AUTO V"
            ),
            QuestionTrivial(
                "¿De qué color es la corbata de Donkey Kong?",
                "ROJA",
                "VERDE",
                "AZUL",
                "NARANJA",
                "ROJA"
            ),
            QuestionTrivial(
                "¿Cómo se llamó el evento de Fortnite que presentó el rapero Travis Scott?",
                "IMAGINATIONLAND",
                "TEMPLE CRAZY",
                "ASTRONOMICAL",
                "RIFT TOUR",
                "ASTRONOMICAL"
            )
        )
    }
}