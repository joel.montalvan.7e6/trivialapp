package com.example.tontoolisto

object TrivialHandler {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "¿A que dia viaja Marty en Regreso al futuro 2?",
                "6 DE ENERO DE 2030",
                "14 DE JULIO DE 2016",
                "21 DE OCTUBRE DE 2015",
                "4 DE NOVIEMBRE DE 2020",
                "21 DE OCTUBRE DE 2015"
            ),

            QuestionTrivial(
                "¿Que actor fue el último en interpretar a Superman?",
                "NICOLAS CAGE",
                "TOM WELLING",
                "HENRY CAVILL",
                "CHRISTOPHER REEVE",
                "HENRY CAVILL"
            ),

            QuestionTrivial(
                "¿Quien dirigio la pelicula Titanic (1998)?",
                "JAMES CAMERON",
                "CHRISTOPHER NOLAN",
                "STEVEN SPIELBERG",
                "QUENTIN TARANTINO",
                "JAMES CAMERON"
            ),
            QuestionTrivial(
                "¿Quien dijo la mítica frase “Un gran poder, conlleva una gran responsabilidad”?",
                "BRUCE WAYNE / BATMAN",
                "PETER PARKER / SPIDER-MAN",
                "BENJAMIN FRANKLIN PARKER",
                "CLARK KENT / SUPERMAN",
                "BENJAMIN FRANKLIN PARKER"
            ),
            QuestionTrivial(
                "¿Cual es la película que más ha recaudado en la historia del cine?",
                "SPIDER-MAN: NO WAY HOME",
                "STAR WARS: EPISODE VII",
                "VENGADORES: ENDGAME",
                "AVATAR",
                "AVATAR"
            ),
            QuestionTrivial(
                "En Harry Potter, ¿Qué hechizo debes conjurar para arreglar unas gafas?",
                "OCULUS REPARO",
                "OCULUS REPARADO",
                "GLAFULUS ARREGLO",
                "CHIMICHANGLUS OCULOREPAR",
                "OCULUS REPARO"
            ),
            QuestionTrivial(
                "¿De qué metal están hechas las garras de Lobezno?",
                "HIERRO",
                "VIBRANIUM",
                "ADAMANTIUM",
                "ACERO",
                "ADAMANTIUM"
            ),
            QuestionTrivial(
                "En 'The Walking Dead' ¿Cómo se llama el bate de Negan?",
                "LUBELLY",
                "LUCILLE",
                "MAX POWER",
                "VAMPYRE",
                "LUCILLE"
            ),
            QuestionTrivial(
                "En Star Wars ¿Cuál es el planeta de Luke Skywalker?",
                "ALDERAAN",
                "NABOO",
                "CORUSCANT",
                "TATOOINE",
                "TATOOINE"
            ),
            QuestionTrivial(
                "¿Por si no nos vemos luego...",
                "(...)NO TE OLVIDES EL PARAGUA",
                "(...)RECUERDA COMPRAR LECHE",
                "(...)BUENOS DÍAS, BUENAS TARDES, BUENAS NOCHES",
                "(...)TODA MI ESPADA, LLEVALA POR MI",
                "(...)BUENOS DÍAS, BUENAS TARDES, BUENAS NOCHES"
            ),
            QuestionTrivial(
                "¿En qué año se estrenó la mítica película Jurassic Park?",
                "1993",
                "1991",
                "1995",
                "1997",
                "1993"
            ),
            QuestionTrivial(
                "¿Cómo se llama la madre de Simba?",
                "NALA",
                "SARABI",
                "MINERVA",
                "OKOYE",
                "SARABI"
            )
        )
    }
}