package com.example.tontoolisto

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.tontoolisto.databinding.FragmentMenuInicialBinding

class MenuInicial : Fragment() {
    lateinit var binding: FragmentMenuInicialBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMenuInicialBinding.inflate(layoutInflater)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.playButton.setOnClickListener {
            var username = binding.usernameImput.text.toString()
            if (username == ""){
                Toast.makeText(context, "Tienes que introducir un nombre de usuario para poder jugar", Toast.LENGTH_SHORT).show()
            }
            else parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,TematicaGame())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }
        binding.rankingButton.setOnClickListener{
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentMenu_Inicial,RankingGame())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }
        }

        binding.ayudaButton.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this.context).create()
            alertDialog.setTitle("Instrucciones")
            alertDialog.setMessage("El juego consiste en responder las diferentes preguntas que te proponemos con un total de 10 segundos por pregunta. Una vez pasado el tiempo, saltarás a la siguiente pregunta."
                    + "Puedes escoger la temática que más te interese, te ves capaz para hacerlas todas?" +
                    "Al finalizar la partida conseguiras unos puntos totales por cada pregunta acertada en su tiempo, estos puntos quedaran registrados en el ranking con el resto de jugadores.")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "ACEPTAR"
            ) { dialog, which -> dialog.dismiss() }
            alertDialog.show()
        }

    }

}