package com.example.tontoolisto

object TrivialHandlerGeografia {

    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "¿Cual es el país más poblado del mundo?",
                "RUSIA",
                "CHINA",
                "ESTADOS UNIDOS",
                "BRASIL",
                "CHINA"
            ),

            QuestionTrivial(
                "¿Cómo se llama el pozo más profundo del mundo localizado en Rusia, el cual fue perforado hacia el interior de la tierra?",
                "POZO DE VRTOGLAVICA",
                "POZO DE JOIDA",
                "POZO DE KOLA",
                "POZO DE EZEIZA",
                "POZO DE KOLA"
            ),

            QuestionTrivial(
                "¿En qué continente se encuentran las montañas más altas del mundo?",
                "EUROPA",
                "AMÉRICA",
                "ÁFRICA",
                "ASIA",
                "ASIA"
            ),
            QuestionTrivial(
                "¿Cuál es el país más pequeño del mundo?",
                "SEALAND",
                "EL VATICANO",
                "MONACO",
                "TUVALU",
                "SEALAND"
            ),
            QuestionTrivial(
                "¿Cuál es la capital de Armenia?",
                "TAIWAN",
                "EREVÁN",
                "REVAN",
                "MATALI",
                "EREVÁN"
            )
        )
    }
}