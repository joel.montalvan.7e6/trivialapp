package com.example.tontoolisto

object TrivialHandlerHistoria {
    fun getQuestions():ArrayList<QuestionTrivial>{

        return arrayListOf(
            QuestionTrivial(
                "La Guerra Fría fue entre Estados Unidos y ¿qué otra potencia mundial?",
                "ITALIA",
                "LA UNIÓN SOVIÉTICA",
                "RUSIA",
                "ALEMANIA",
                "LA UNIÓN SOVIÉTICA"
            ),

            QuestionTrivial(
                "¿Quién fue la primera ministra de la Gran Bretaña?",
                "QUEEN ELIZABETH II",
                "FLORENCE NIGHTINGALE",
                "MARGARET THATCHER",
                "JANE AUSTEN",
                "MARGARET THATCHER"
            ),

            QuestionTrivial(
                "¿De qué partido era líder Adolf Hitler?",
                "FRANQUISTA",
                "URSS",
                "COMUNISTA",
                "ÁTOMO",
                "NAZI"
            ),
            QuestionTrivial(
                "¿Qué continente fue devastado por la peste negra en el siglo XIV?",
                "EUROPA",
                "ASIA",
                "ÁFRICA",
                "AMÉRICA",
                "EUROPA"
            ),
            QuestionTrivial(
                "¿En qué año comenzó la Primera Guerra Mundial?",
                "1913",
                "1914",
                "1917",
                "1915",
                "1914"
            )
        )
    }
}